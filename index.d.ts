type ListenCallback = (message: any, respond: (error: Error, message: any) => void) => void;

type SubscribeCallback = (message: any, reply: string, subject: string) => void;

type ProcessCallback = (message: any, subject: string) => void;

type RequestCallback = (error: Error, response: any) => void;

declare class NatsBroker {

    constructor(options: Options);

    /** Publish a message */
    publish(subject: string, message: any): void;

    /** Subscribe to point-to-point requests */
    listen(subject: string, done: ListenCallback): void;

    /** Subscribe to broadcasts */
    subscribe(subject: string, done: SubscribeCallback): void;

    /** Subscribe as queue worker */
    process(subject: string, done: ProcessCallback): void;

    /** Publish a message and wait for the first response */
    request(subject: string, message: any, done: RequestCallback): void;

    /** Close underlying connection with NATS */
    close(): void;

}

interface Options {
    
    requestTimeout?: number
    
    url?: string
    
    group?: string
}

export = NatsBroker;
