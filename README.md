NATS Client for JavaScript
===========================




Installation
------------

dd dependency to the package.json :

 ```"nats-client": "bitbucket:devorchestra/nats-client#v1.0.0"```

Usage
-----

Create an instance of the wrapper:

```
let NATS = require('nats-client');

const nats = NATS({group: 'some-service', requestTimeout: 5000});
```


Publish a request and get a response in callback:

```
nats.request('any.request', {id: 'temp'}, (error, data) => {
    console.log(error, data)
});
```


Subscribe and respond to a request:

```
nats.listen('any.request', ({id}, respond) => {
    someModule.fetch(id, (error, data) => {
        respond(error, data);
    });
});
```


Publish an event:

```
nats.publish('event.occurred', data);
```

Subscribe to the event

```
nats.subscribe('event.occurred', (data) => {
    console.log(subject, data);
});
```

Subscribe and process as worker queue:

```
nats.process('event.occurred', (data) => {
    console.log(subject, data);
});
```

Close NATS connection (if needed):

```
nats.close();
```
