const NATS = require('nats');
const Logger = require('./logger');
const merge = require('lodash/merge');

class NatsBroker {
    constructor(options) {
        const defaults = {
            requestTimeout: 10000,
            group: 'default'
        };
        this._options = options ? merge(defaults, options) : defaults;
        this._logger = Logger(this._options.group);
        this._nats = NATS.connect(this._options);
        this._logger.info('connected to NATS:', this._nats.currentServer.url.host);
        this._logger.info('instance group is:', this._options.group);
    }

    // close underlying connection with NATS
    close() {
        this._logger.info('closing connection with NATS:', this._nats.currentServer.url.host);
        this._nats.close();
    };


    // REQUEST-RESPONSE PATTERN

    // request for the response
    request(subject, message, done) {
        const self = this;
        self._logger.debug(`request to '${subject}' with message: ${JSON.stringify(message)}`);
        self._nats.requestOne(subject, JSON.stringify(message), null, self._options.requestTimeout, function (response) {
            if (response.code && response.code === NATS.REQ_TIMEOUT) {
                self._logger.error('response timeout');
                return done(new Error('response timeout'))
            }
            let parsedResponse = JSON.parse(response);
            const error = parsedResponse[0];
            const res = parsedResponse[1];
            if (error) {
                self._logger.error('request ended with error:', error.message || error.detail);
                return done(new Error(error.message || error.detail));
            }
            self._logger.debug(`request ended with response: ${response}`);
            return done(null, res);
        });
    };

    // subscribe to request which will be given a response
    listen(subject, done) {
        const self = this;
        const group = subject + '.listeners';
        self._logger.debug(`subscribing to requests ${subject} as member of ${group}`);
        self._nats.subscribe(subject, {queue: group}, function (message, reply, subject) {
            self._logger.debug(`received a request '${subject}' with message: ${message}`);
            done(JSON.parse(message), self._respond(reply));
        })
    };

    // returned by `listen`, not to be used directly
    _respond(replyTo) {
        const self = this;
        return function (error, response) {
            if (error) {
                self._logger.error(`sending error response to ${replyTo}: ${error}`);
                self._nats.publish(replyTo, JSON.stringify([{message: error.message, stack: error.stack}]));
            } else {
                self._logger.debug(`sending response to ${replyTo} with message ${JSON.stringify(response)}`);
                self._nats.publish(replyTo, JSON.stringify([null, response]));
            }
        }
    };


    // PUBLISH-SUBSCRIBE PATTERN

    // publish a message for the subscribed listeners
    publish(subject, message) {
        const self = this;
        self._logger.debug('publishing to', subject, message);
        self._nats.publish(subject, JSON.stringify(message));
    };

    // subscribe to broadcasts
    subscribe(subject, done) {
        const self = this;
        self._logger.debug('subscribing to broadcasts', subject);
        self._nats.subscribe(subject, function (message, reply, subject) {
            self._logger.debug('received a broadcast', subject, 'with', message);
            done(JSON.parse(message), reply, subject);
        })
    };


    // QUEUEING PATTERN
    // use self.publish method to publish a message to the queue

    // subscribe to topic as a queue worker
    process(subject, done) {
        const self = this;
        const group = subject + '.workers.' + self._options.group;
        self._logger.debug('subscribing to process', subject, 'queue as member of', group);
        self._nats.subscribe(subject, {queue: group}, function (message, reply, subject) {
            self._logger.debug('processing', subject, 'with', message);
            done(JSON.parse(message), subject);
        })
    };
}

module.exports = NatsBroker;
